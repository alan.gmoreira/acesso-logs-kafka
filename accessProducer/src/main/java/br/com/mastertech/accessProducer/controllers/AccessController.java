package br.com.mastertech.accessProducer.controllers;

import br.com.mastertech.accessProducer.services.AccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.BooleanLiteral;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accessAttempt")
public class AccessController {

    @Autowired
    private AccessService accessService;

    @GetMapping("/{customerId}/{doorId}")
    public boolean getAccess(@PathVariable int customerId, @PathVariable int doorId) {
        return accessService.tryAccess(customerId, doorId);
    }
}
