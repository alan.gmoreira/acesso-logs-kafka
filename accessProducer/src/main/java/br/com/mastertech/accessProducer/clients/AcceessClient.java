package br.com.mastertech.accessProducer.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ACCESS", configuration = AccessConfiguration.class)
public interface AcceessClient {

    @GetMapping("/acesso/{cliente_id}/{porta_id}")
    public Access getAccess(@PathVariable int cliente_id, @PathVariable int porta_id);
}
