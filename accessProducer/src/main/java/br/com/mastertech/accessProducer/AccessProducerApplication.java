package br.com.mastertech.accessProducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AccessProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccessProducerApplication.class, args);
	}

}
