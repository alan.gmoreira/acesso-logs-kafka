package br.com.mastertech.accessProducer;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class AccessLog {

    private int doorId;
    private int customerId;
    private LocalDateTime accessDatetime;
    private boolean success;

    public AccessLog() {
    }

    public int getDoorId() {
        return doorId;
    }

    public void setDoorId(int doorId) {
        this.doorId = doorId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public LocalDateTime getAccessDatetime() {
        return accessDatetime;
    }

    public void setAccessDatetime(LocalDateTime accessDatetime) {
        this.accessDatetime = accessDatetime;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
