package br.com.mastertech.accessProducer.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class AccessConfiguration {

    @Bean
    public ErrorDecoder getAccessDecoder() {
        return new AccessDecoder();
    }

}
