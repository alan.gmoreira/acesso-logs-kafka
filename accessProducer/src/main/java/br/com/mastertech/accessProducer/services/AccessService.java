package br.com.mastertech.accessProducer.services;

import br.com.mastertech.accessProducer.AccessLog;
import br.com.mastertech.accessProducer.clients.AcceessClient;
import br.com.mastertech.accessProducer.clients.Access;
import br.com.mastertech.accessProducer.exceptions.AccessNotFoundException;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.FieldAccessor_Boolean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;

@Service
public class AccessService {

    @Autowired
    private KafkaTemplate<String, AccessLog> accessProducer;

    @Autowired
    private AcceessClient acceessClient;

    //spec4-alan-garcia-1

    private boolean validateAccess(int customerId, int doorId)
    {
        try {
            Access accessTry = acceessClient.getAccess(customerId, doorId);
            return true;
        }
        catch (AccessNotFoundException e)
        {
            return false;
        }
    }

    public boolean tryAccess(int customerId, int doorId)
    {
        boolean success = validateAccess(customerId, doorId);

        AccessLog accessLog = new AccessLog();

        accessLog.setAccessDatetime(LocalDateTime.now());
        accessLog.setCustomerId(customerId);
        accessLog.setDoorId(doorId);
        accessLog.setSuccess(success);

        accessProducer.send("spec4-alan-garcia-1", accessLog);

        return success;
    }

}
