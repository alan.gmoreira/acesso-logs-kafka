package br.com.mastertech.accessProducer.clients;

import br.com.mastertech.accessProducer.exceptions.AccessNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class AccessDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404)
            return new AccessNotFoundException();

        return errorDecoder.decode(s, response);
    }
}
