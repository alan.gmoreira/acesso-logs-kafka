package br.com.mastertech.accessProducer;

import java.time.LocalDateTime;

public class AccessLog {
    private int doorId;
    private int customerId;
    private LocalDateTime accessDatetime;
    private boolean success;

    public AccessLog() {
    }

    public int getDoorId() {
        return doorId;
    }

    public void setDoorId(int doorId) {
        this.doorId = doorId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public LocalDateTime getAccessDatetime() {
        return accessDatetime;
    }

    public void setAccessDatetime(LocalDateTime accessDatetime) {
        this.accessDatetime = accessDatetime;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    public static String toHeaderCSV()
    {
        return  "customerId,doorId,sucesso,Data";
    }

    public String toDataCSV()
    {
        return customerId + "," + doorId + "," + success + "," + accessDatetime;
    }

    @Override
    public String toString() {
        return "customerId: " + customerId + " doorId: " + doorId + " sucesso: " + success + " Data: " + accessDatetime;
    }
}
