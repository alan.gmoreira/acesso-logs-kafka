package br.com.mastertech.accessConsumer;

import br.com.mastertech.accessProducer.AccessLog;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

@Component
public class AccessConsumer {
    @KafkaListener(topics = "spec4-alan-garcia-1", groupId = "AlanMoreira")
    public void receber(@Payload AccessLog accessLog) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Writer writer = new FileWriter("D:\\acessos.csv");

        // mapping of columns with their positions
        ColumnPositionMappingStrategy<AccessLog> mappingStrategy = new ColumnPositionMappingStrategy<AccessLog>();
        // Set mappingStrategy type to Product Type
        mappingStrategy.setType(AccessLog.class);
        // Fields in Product Bean
        String[] columns = new String[] { "productCode", "MFD", "EXD" };
        // Setting the colums for mappingStrategy
        mappingStrategy.setColumnMapping(columns);

        StatefulBeanToCsvBuilder<AccessLog> builder = new StatefulBeanToCsvBuilder<AccessLog>(writer);

        StatefulBeanToCsv<AccessLog> beanWriter = builder.withMappingStrategy(mappingStrategy).build();
        // Writing data to csv file
        beanWriter.write(accessLog);
        writer.close();

        System.out.println("Mensagem: " + accessLog.toString());
    }
}
