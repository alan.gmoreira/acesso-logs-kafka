package br.com.mastertech.accessConsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccessConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccessConsumerApplication.class, args);
	}

}
